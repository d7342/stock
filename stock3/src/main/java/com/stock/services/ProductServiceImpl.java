package com.stock.services;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.stock.entities.Product;
import com.stock.entities.dto.ProductDTO;
import com.stock.entities.dto.ProductFormDTO;
import com.stock.repositories.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductRepository repository;

	@Autowired
	private ModelMapper mapper;

	@Override
	public ProductDTO saveProduct(ProductFormDTO body) {
		
		return mapper.map(repository.save(mapper.map(body, Product.class)), ProductDTO.class);
	}

	@Override
	public Page<ProductDTO> listProducts(PageRequest pageRequest) {
		Page<Product> page = repository.findAll(pageRequest);

		List<ProductDTO> list = page.getContent().stream().map(Product -> mapper.map(Product, ProductDTO.class))
				.collect(Collectors.toList());
		return new PageImpl<>(list);
	}

	@Override
	public ProductDTO updateProduct(Long id, ProductFormDTO body) {
		Product product = repository.findById(id)
				.orElseThrow(() -> new com.stock.exceptions.ResourceNotFoundException("Id not found " + id));
		product.setName(body.getName());
		product.setMinStock(body.getMinStock());
		product.setUnity(body.getUnity());
		product.setPrice(body.getPrice());
		
		return mapper.map(repository.save(product), ProductDTO.class);

	}

	@Override
	public ProductDTO findById(Long id) {
		Product product = repository.findById(id)
				 .orElseThrow(() -> new com.stock.exceptions.ResourceNotFoundException("Id not found " + id));
		return mapper.map(product, ProductDTO.class);
	}

	@Override
	public void deleteProduct(Long id) {
		Product product  = repository.findById(id)
				 .orElseThrow(() -> new com.stock.exceptions.ResourceNotFoundException("Id not found " + id));
		repository.delete(product);

	}

}
