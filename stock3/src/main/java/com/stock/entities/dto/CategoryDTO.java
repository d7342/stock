package com.stock.entities.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.stock.entities.Category;

import lombok.Data;

@Data
public class CategoryDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Long id;
	@Size(min=5, max= 150, message="O onome deve conter de 5 a 150 caracteres")
	@NotBlank(message= "Campo Obrigatório")
	private String name;
	
	public CategoryDTO(Category entity) {
		this.id = entity.getId();
		this.name = entity.getName();
	}
}
