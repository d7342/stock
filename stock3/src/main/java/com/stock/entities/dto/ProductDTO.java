package com.stock.entities.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.stock.entities.Category;
import com.stock.entities.Product;

import lombok.Data;

@Data
public class ProductDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Long id;
	@Size(min=5, max= 150, message="O onome deve conter de 5 a 150 caracteres")
	@NotBlank(message= "Campo Obrigatório")
	private String name;
	@Positive(message= "Preço deve ser um valor positivo")
	private double price;
	@NotBlank(message= "Campo Obrigatório")
	private String unity;
	@Positive(message= "Estoque deve ser um valor positivo")
	private double minStock;
	
	private List<CategoryDTO> categories = new ArrayList<>();
	
	public ProductDTO(Product entity) {
		this.id = entity.getId();
		this.name = entity.getName();
		this.minStock = entity.getMinStock();
		this.price = entity.getPrice();
		this.unity = entity.getUnity();
	}
	
	public ProductDTO(Product entity, Set<Category> categories) {
		this(entity);
		categories.forEach(cat -> this.categories.add(new CategoryDTO(cat)));
	}
}
